FROM openjdk:8-jre-alpine
COPY build/libs/addressbook-0.0.1-SNAPSHOT.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]
