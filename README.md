# Address-book
This is a spring boot based REST Api to implement use cases of a standard address book.

## Features and technologies demonstrated
- Spring boot
- Springfox
- Database
  - H2: for test
  - Postgres: for local dev and prod
- Flyway migration
- Docker
- Gradle

## Steps to run
Please follow below steps to run locally and in docker

### 1. Build
`./gradlew clean build`

Above command will run tests using H2 and produce an executable jar under `build/libs/`

### 2. Run app and db in docker
`docker-compose up`

Above command will first start Postgres db docker container. After this, the spring boot application executable jar (produced in previous step) will be deployed on another docker container. The spring boot application will run in `prod` profile on port `8080`.

### 3. Run app on local machine
`./gradlew bootRun --args='--spring.profiles.active=dev'`

Above command will start the spring boot application on localhost. The spring boot application will run in `dev` profile on port `8081`.

### 4. Swagger for REST Api
`http://localhost:8080/swagger-ui/`

### 5. Actuator health
`http://localhost:8080/actuator/health`

### Note
1. Application from localhost will also connect to the same Postgres db on docker container
2. Todos are added in place for areas of improvement

