package au.com.reece.addressbook.model;

import lombok.Data;

@Data
public class ContactDao {

    private String name;

    private String number;

}
