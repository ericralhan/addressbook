package au.com.reece.addressbook.controller;

import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// TODO: ResponseEntity can be used to build response
// TODO: HttpStatus codes can be used to reflect response status
// TODO: can reduce coupling with entity objects by using objectMapper
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(value = "/{id}")
    public User getUser(@PathVariable("id") int id) {
        return userService.getUserById(id);
    }

    @DeleteMapping(value = "/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        userService.deleteUserById(id);
        return "{\"status:\":\"success\"}";
    }

    @PostMapping(value = "/{name}")
    public String addUser(@PathVariable("name") String name) {
        userService.addUser(name);
        return "{\"status:\":\"success\"}";
    }

    @GetMapping(value = "/userByName/{name}")
    public List<User> getUserByName(@PathVariable("name") String name) {
        return userService.getUsersByName(name);
    }

}
