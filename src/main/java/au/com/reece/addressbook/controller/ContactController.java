package au.com.reece.addressbook.controller;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.model.ContactDao;
import au.com.reece.addressbook.service.ContactService;
import au.com.reece.addressbook.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

// TODO: ResponseEntity can be used to build response
// TODO: HttpStatus codes can be used to reflect response status
// TODO: can reduce coupling with entity objects by using objectMapper
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/contact")
public class ContactController {

    private final ContactService contactService;

    private final UserService userService;

    @GetMapping
    public List<Contact> getAllContact() {
        return contactService.getAllContacts();
    }

    @GetMapping(value = "/{contactId}")
    public Contact getContact(@PathVariable("contactId") int id) {
        return contactService.getContact(id);
    }

    @PostMapping(value = "/{userId}")
    public String saveContacts(@PathVariable("userId") int id, @RequestBody List<ContactDao> contactDaoList) {
        // TODO: logic of mapping ContactDao to Contact can move to service
        // TODO: objectMapper/ mapping service can be used to perform below mapping
        List<Contact> contactList = new ArrayList<>();
        User user = userService.getUserById(id);
        for (ContactDao contactDao : contactDaoList) {
            Contact contact = new Contact();
            contact.setName(contactDao.getName());
            contact.setNumber(contactDao.getNumber());
            contact.setUser(user);
            contactList.add(contact);
        }

        contactService.saveAllContact(contactList);

        return "{\"status:\":\"success\"}";
    }

    @PutMapping(value = "/{contactId}")
    public String updateContact(@PathVariable("contactId") int id, @RequestBody ContactDao contactDao) {
        // TODO: logic of mapping ContactDao to Contact can move to service
        // TODO: objectMapper/ mapping service can be used to perform below mapping
        Contact contact = new Contact();
        contact.setId(id);
        contact.setName(contactDao.getName());
        contact.setNumber(contactDao.getNumber());
        contact.setUser(contactService.getContact(id).getUser());

        contactService.updateContact(contact);

        return "{\"status:\":\"success\"}";
    }

    @DeleteMapping(value = "/{contactId}")
    public String deleteContact(@PathVariable("contactId") int id) {
        contactService.deleteContactById(id);
        return "{\"status:\":\"success\"}";
    }

    @GetMapping(value = "/contactsByName/{name}")
    public List<Contact> getContactsByName(@PathVariable("name") String name) {
        // TODO: exposing entity object in response makes controller layer tightly coupled with entity. This can be avoided.
        // TODO: objectMapper/ mapping service can be used to perform mapping
        return contactService.getContactsByName(name);
    }

    @GetMapping(value = "/contactsByNum/{number}")
    public List<Contact> getContactsByNumber(@PathVariable("number") String number) {
        // TODO: exposing entity object in response makes controller layer tightly coupled with entity. This can be avoided.
        // TODO: objectMapper/ mapping service can be used to perform mapping
        return contactService.getContactsByNumber(number);
    }

    @GetMapping(value = "/contactsByUser/{userId}")
    public List<Contact> getContactsByUserId(@PathVariable("userId") int id) {
        // TODO: exposing entity object in response makes controller layer tightly coupled with entity. This can be avoided.
        // TODO: objectMapper/ mapping service can be used to perform mapping
        return contactService.getContactsByUserId(id);
    }

    @GetMapping(value = "/unique")
    public List<Contact> getUniqueContacts() {
        return contactService.getAllUniqueContacts();
    }

}
