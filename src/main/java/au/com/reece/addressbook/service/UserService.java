package au.com.reece.addressbook.service;

import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int id) {
        return userRepository.getOne(id);
    }

    public List<User> getUsersByName(String name) {
        return userRepository.getByName(name);
    }

    public void addUser(String name) {
        User user = new User();
        user.setName(name);
        userRepository.save(user);
    }

    public void deleteUserById(int id) {
        userRepository.deleteById(id);
    }
}
