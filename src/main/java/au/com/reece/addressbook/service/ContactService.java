package au.com.reece.addressbook.service;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;

    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    public List<Contact> getContactsByName(String name) {
        return contactRepository.getByName(name);
    }

    public List<Contact> getContactsByNumber(String number) {
        return contactRepository.getByNumber(number);
    }

    public List<Contact> getContactsByUserId(int id) {
        return contactRepository.getByUserId(id);
    }

    public void deleteContactById(int id) {
        contactRepository.deleteById(id);
    }

    public void updateContact(Contact contact) {
        contactRepository.save(contact);
    }

    public Contact getContact(int contactId) {
        return contactRepository.getOne(contactId);
    }

    public void saveAllContact(List<Contact> contactList) {
        contactRepository.saveAll(contactList);
    }

    public List<Contact> getAllUniqueContacts() {
        return contactRepository.findDistinctContacts();
    }
}
