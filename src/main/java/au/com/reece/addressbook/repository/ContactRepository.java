package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    List<Contact> getByName(String name);

    List<Contact> getByNumber(String number);

    List<Contact> getByUserId(int id);

    @Query(value = "SELECT DISTINCT new au.com.reece.addressbook.entity.Contact (c.name, c.number)  FROM Contact c")
    List<Contact> findDistinctContacts();
}
