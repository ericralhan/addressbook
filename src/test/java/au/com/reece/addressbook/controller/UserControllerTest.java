package au.com.reece.addressbook.controller;

import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static au.com.reece.addressbook.testHelper.UserMock.getTestUser;
import static au.com.reece.addressbook.testHelper.UserMock.getTestUsers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void testAddUser() throws Exception {
        String result = "{\"status:\":\"success\"}";

        doNothing().when(userService).addUser("testMockUser");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/testMockUser");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetUserByName() throws Exception {
        List<User> userList = getTestUsers();
        String result = "[{\"id\":1,\"name\":\"testMockUser\",\"contactSet\":[]}]";

        when(userService.getUsersByName("testMockUser")).thenReturn(userList);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/userByName/testMockUser");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetAllUsers() throws Exception {
        List<User> userList = getTestUsers();
        String result = "[{\"id\":1,\"name\":\"testMockUser\",\"contactSet\":[]}]";

        when(userService.getAllUsers()).thenReturn(userList);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetUserById() throws Exception {
        User user = getTestUser();
        String result = "{\"id\":1,\"name\":\"testMockUser\",\"contactSet\":[]}";
        when(userService.getUserById(1)).thenReturn(user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/1");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testDeleteContactById() throws Exception {
        String result = "{\"status:\":\"success\"}";
        doNothing().when(userService).deleteUserById(anyInt());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/user/1");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }
}
