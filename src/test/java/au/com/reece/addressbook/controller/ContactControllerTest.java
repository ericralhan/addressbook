package au.com.reece.addressbook.controller;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.service.ContactService;
import au.com.reece.addressbook.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static au.com.reece.addressbook.testHelper.ContactMock.getTestContact;
import static au.com.reece.addressbook.testHelper.ContactMock.getTestContacts;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = ContactController.class)
public class ContactControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ContactService contactService;

    @MockBean
    private UserService userService;

    @Test
    public void testGetContactsByName() throws Exception {
        List<Contact> contactList = getTestContacts(mock(User.class));
        String result = "[{\"id\":1,\"name\":\"john\",\"number\":\"01112208496\"}]";

        when(contactService.getContactsByName("john")).thenReturn(contactList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/contact/contactsByName/john");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.OK.value(), response.getResponse().getStatus());
        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetContactsByNum() throws Exception {
        List<Contact> contactList = getTestContacts(mock(User.class));

        String result = "[{\"id\":1,\"name\":\"john\",\"number\":\"01112208496\"}]";

        when(contactService.getContactsByNumber("01112208496")).thenReturn(contactList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/contact/contactsByNum/01112208496");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetContactsByUserId() throws Exception {
        List<Contact> contactList = getTestContacts(mock(User.class));

        String result = "[{\"id\":1,\"name\":\"john\",\"number\":\"01112208496\"}]";

        when(contactService.getContactsByUserId(1)).thenReturn(contactList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/contact/contactsByUser/1");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testGetContactById() throws Exception {
        Contact contact = getTestContact(mock(User.class));
        String result = "{\"id\":1,\"name\":\"john\",\"number\":\"01112208496\"}";

        when(contactService.getContact(1)).thenReturn(contact);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/contact/1");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testDeleteContactById() throws Exception {
        String result = "{\"status:\":\"success\"}";
        doNothing().when(contactService).deleteContactById(anyInt());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/contact/1");
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testUpdateContact() throws Exception {
        Contact contact = getTestContact(mock(User.class));
        String contactDaoJson = "{\"name\":\"john\",\"number\":\"01112208496\"}";
        String result = "{\"status:\":\"success\"}";

        doNothing().when(contactService).updateContact(isA(Contact.class));
        when(contactService.getContact(anyInt())).thenReturn(contact);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/contact/1").content(contactDaoJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult response = mockMvc.perform(requestBuilder).andReturn();

        assertEquals(result, response.getResponse().getContentAsString());
    }

    @Test
    public void testSaveContacts() throws Exception {
        String contactDaoJson = "[{\"name\":\"john\",\"number\":\"01112208496\"}]";
        String result = "{\"status:\":\"success\"}";
        doNothing().when(contactService).saveAllContact(anyList());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/contact/1").content(contactDaoJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult response = mockMvc.perform(requestBuilder).andReturn();
        assertEquals(result, response.getResponse().getContentAsString());
    }
}
