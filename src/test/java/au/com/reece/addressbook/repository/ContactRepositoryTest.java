package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static au.com.reece.addressbook.testHelper.ContactMock.getTestContact;
import static au.com.reece.addressbook.testHelper.UserMock.getTestUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ContactRepositoryTest {

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    public void init() {
        Contact contact = new Contact();
        contact.setName("test contact");
        contact.setNumber("0112208496");

        User user = new User();
        user.setName("test user");

        userRepository.save(user);
        contact.setUser(user);
        contactRepository.save(contact);
    }

    @Test
    public void testGetByName() {
        assertNotNull(contactRepository.getByName("test contact"));
    }

    @Test
    public void testGetByNumber() {
        assertNotNull(contactRepository.getByNumber("0112208496"));
    }

    @Test
    public void testGetByUserId() {
        User user = getTestUser();
        assertNotNull(contactRepository.getByUserId(user.getId()));
    }

    @Test
    public void findDistinctNameAndNumber_whenCalled_returnsUniqueContactList() {
        // Arrange
        Contact contactA = new Contact();
        contactA.setName("test contact blah");
        contactA.setNumber("0772208496");

        Contact contactB = new Contact();
        contactB.setName("test contact blah");
        contactB.setNumber("0772208497");

        Contact contactX = new Contact();
        contactX.setName("test contact blah");
        contactX.setNumber("0772208496");

        Contact contactY = new Contact();
        contactY.setName("test contact blah");
        contactY.setNumber("0772208497");

        User user1 = new User();
        user1.setName("test user - 1");

        User user2 = new User();
        user2.setName("test user - 2");

        userRepository.save(user1);
        userRepository.save(user2);

        contactA.setUser(user1);
        contactB.setUser(user1);
        contactRepository.save(contactA);
        contactRepository.save(contactB);

        contactX.setUser(user2);
        contactY.setUser(user2);
        contactRepository.save(contactX);
        contactRepository.save(contactY);

        List<Contact> contactList = contactRepository.findDistinctContacts();

        assertEquals(3, contactList.size());
    }

}
