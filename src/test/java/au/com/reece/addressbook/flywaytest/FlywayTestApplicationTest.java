package au.com.reece.addressbook.flywaytest;

import au.com.reece.addressbook.repository.UserRepository;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class FlywayTestApplicationTest {

    @Autowired
    UserRepository userRepository;

    @FlywayTest
    @Test
    public void cleanDBFlywayTest() {
        userRepository.findAll();
        assertEquals(0, userRepository.findAll().size());
    }

    @FlywayTest(locationsForMigrate = {"/h2"})
    @Test
    public void flywayTest() {
        assertNotNull(userRepository.getByName("flyway test unit"));
    }
}
