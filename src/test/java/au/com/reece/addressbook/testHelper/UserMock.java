package au.com.reece.addressbook.testHelper;

import au.com.reece.addressbook.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserMock {

    public static List<User> getTestUsers() {
        List<User> userList = new ArrayList<>();
        User user = getTestUser();
        userList.add(user);
        return userList;
    }

    public static User getTestUser() {
        User user = new User();
        user.setId(1);
        user.setName("testMockUser");
        return user;
    }
}
