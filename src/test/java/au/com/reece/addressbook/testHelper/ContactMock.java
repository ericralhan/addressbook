package au.com.reece.addressbook.testHelper;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.entity.User;

import java.util.ArrayList;
import java.util.List;

public class ContactMock {

    // test data setup
    public static List<Contact> getTestContacts(User user) {
        List<Contact> contactList = new ArrayList<>();
        Contact contact = getTestContact(user);
        contactList.add(contact);
        return contactList;
    }

    public static Contact getTestContact(User user) {
        Contact contact = new Contact();
        contact.setId(1);
        contact.setName("john");
        contact.setNumber("01112208496");
        contact.setUser(user);
        return contact;
    }

}
