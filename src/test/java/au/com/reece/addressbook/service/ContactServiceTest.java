package au.com.reece.addressbook.service;

import au.com.reece.addressbook.entity.Contact;
import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static au.com.reece.addressbook.testHelper.ContactMock.getTestContact;
import static au.com.reece.addressbook.testHelper.ContactMock.getTestContacts;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class ContactServiceTest {

    @Spy
    ContactRepository contactRepository;

    @InjectMocks
    ContactService contactService;

    @Captor
    private ArgumentCaptor<Integer> integerCaptor;

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Captor
    private ArgumentCaptor<List<Contact>> contactListCaptor;

    @Test
    public void testGetAllContacts() {
        List<Contact> contactList = getTestContacts(mock(User.class));

        when(contactRepository.findAll()).thenReturn(contactList);

        assertNotEquals(0, contactService.getAllContacts().size());
    }

    @Test
    public void testGetContactsByName() {
        List<Contact> contactList = getTestContacts(mock(User.class));

        when(contactRepository.getByName(anyString())).thenReturn(contactList);

        assertNotEquals(0, contactService.getContactsByName("con").size());
    }

    @Test
    public void testGetContactsByNumber() {
        List<Contact> contactList = getTestContacts(mock(User.class));

        when(contactRepository.getByNumber(anyString())).thenReturn(contactList);

        assertNotEquals(0, contactService.getContactsByNumber("312312").size());
    }

    @Test
    public void testGetContactsByUserId() {
        List<Contact> contactList = getTestContacts(mock(User.class));

        when(contactRepository.getByUserId(anyInt())).thenReturn(contactList);

        assertNotEquals(0, contactService.getContactsByUserId(1).size());
    }

    @Test
    public void testDeleteContactById() {
        doNothing().when(contactRepository).deleteById(anyInt());

        contactService.deleteContactById(134234);

        verify(contactRepository, times(1)).deleteById(integerCaptor.capture());
        assertEquals(134234, integerCaptor.getValue());
    }

    @Test
    public void testUpdateContact() {
        Contact contact = getTestContact(mock(User.class));
        when(contactRepository.save(any(Contact.class))).thenReturn(contact);

        contactService.updateContact(contact);

        verify(contactRepository, times(1)).save(contactCaptor.capture());
        assertEquals(contact.getName(), contactCaptor.getValue().getName());
        assertEquals(contact.getNumber(), contactCaptor.getValue().getNumber());
    }

    @Test
    public void testGetContact() {
        Contact contact = getTestContact(mock(User.class));
        when(contactRepository.getOne(anyInt())).thenReturn(contact);

        Contact resultContact = contactService.getContact(1);

        assertEquals(contact.getName(), resultContact.getName());
        assertEquals(contact.getNumber(), resultContact.getNumber());
    }

    @Test
    public void testSaveAllContact() {
        List<Contact> contacts = getTestContacts(mock(User.class));
        when(contactRepository.saveAll(anyList())).thenReturn(contacts);

        contactService.saveAllContact(contacts);

        verify(contactRepository, times(1)).saveAll(contactListCaptor.capture());
        assertEquals(contacts.size(), contactListCaptor.getValue().size());
        assertEquals(contacts.get(0).getName(), contactListCaptor.getValue().get(0).getName());
        assertEquals(contacts.get(0).getNumber(), contactListCaptor.getValue().get(0).getNumber());
    }

    @Test
    public void testGetAllUniqueContacts() {
        List<Contact> contacts = getTestContacts(mock(User.class));
        when(contactRepository.findDistinctContacts()).thenReturn(contacts);

        List<Contact> resultContacts = contactService.getAllUniqueContacts();

        assertEquals(contacts.size(), resultContacts.size());
        assertEquals(contacts.get(0).getName(), resultContacts.get(0).getName());
        assertEquals(contacts.get(0).getNumber(), resultContacts.get(0).getNumber());
    }
}
