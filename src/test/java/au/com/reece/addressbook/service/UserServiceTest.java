package au.com.reece.addressbook.service;

import au.com.reece.addressbook.entity.User;
import au.com.reece.addressbook.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static au.com.reece.addressbook.testHelper.UserMock.getTestUser;
import static au.com.reece.addressbook.testHelper.UserMock.getTestUsers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @Spy
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @Captor
    private ArgumentCaptor<Integer> integerCaptor;

    @Captor
    private ArgumentCaptor<User> userCaptor;

    @Test
    public void testGetAllUsers() {
        List<User> userList = getTestUsers();
        when(userRepository.findAll()).thenReturn(userList);

        List<User> resultUserList = userService.getAllUsers();

        assertNotEquals(0, resultUserList.size());
    }

    @Test
    public void testGetUserById() {
        User user = getTestUser();
        when(userRepository.getOne(anyInt())).thenReturn(user);

        User resultUser = userService.getUserById(1);

        assertEquals(user.getName(), resultUser.getName());
    }

    @Test
    public void testGetUsersByName() {
        List<User> userList = getTestUsers();
        when(userRepository.getByName(anyString())).thenReturn(userList);

        List<User> resultUserList = userService.getUsersByName("any user");

        assertNotEquals(0, resultUserList.size());
    }

    @Test
    public void testAddUser() {
        User user = getTestUser();
        when(userRepository.save(any(User.class))).thenReturn(user);

        userService.addUser("testMockUser");

        verify(userRepository, times(1)).save(userCaptor.capture());
        assertEquals(user.getName(), userCaptor.getValue().getName());

    }

    @Test
    public void testDeleteUserById() {
        User user = getTestUser();
        doNothing().when(userRepository).deleteById(anyInt());

        userService.deleteUserById(123123);

        verify(userRepository, times(1)).deleteById(integerCaptor.capture());
        assertEquals(123123, integerCaptor.getValue());

    }

}
