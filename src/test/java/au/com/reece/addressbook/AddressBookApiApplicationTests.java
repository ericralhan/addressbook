package au.com.reece.addressbook;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

// TODO: RestAssured based integration tests can be added for e2e testing
// TODO: testContainer (postgres) can be used to make test environment closer to prod
@ActiveProfiles("test")
@SpringBootTest
class AddressBookApiApplicationTests {

    @Test
    void contextLoads() {
    }

}
