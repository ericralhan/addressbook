CREATE SCHEMA IF NOT EXISTS test_schema;

CREATE TABLE IF NOT EXISTS test_schema.users
(
  user_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS test_schema.contact
(
  contact_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  contact_name VARCHAR(255),
  contact_number VARCHAR(255),
  user_id INTEGER,
 FOREIGN KEY(user_id)  references users(user_id) ON DELETE CASCADE
);

